const command = document.getElementById("command");
const output = document.getElementById("output");
const result = document.getElementById("result");
const button = document.getElementById("execute");
const interfaceCache = {};
const interfaceStatusCache = {};
const interfaceVlansCache = {};
let isPopupSticky = false;

function populate_interface_vlans_cache() {
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", "show vlans"])
        .then(data => {
            parse_vlans_data(data);
            console.log("VLANs data parsed and cached:", interfaceVlansCache); // For debugging purposes
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
}

function parse_vlans_data(data) {
    const lines = data.split('\n');
    let currentVlan = null;

    lines.forEach(line => {
        const trimmedLine = line.trim();
        if (trimmedLine) {
            const parts = trimmedLine.split(/\s+/);
            if (parts.length === 2) {
                currentVlan = {
                    name: parts[0],
                    tag: parts[1] === '' ? 0 : parseInt(parts[1], 10),
                    interfaces: []
                };
                interfaceVlansCache[currentVlan.name] = currentVlan;
            } else if (currentVlan && trimmedLine.includes('ge-')) {
                const interfaces = trimmedLine.split(',').map(intf => intf.trim().replace(/\.0\*?/, ''));
                currentVlan.interfaces.push(...interfaces);
            }
        }
    });
}

function setCursorBusy(isBusy) {
    document.body.style.cursor = isBusy ? 'wait' : 'default';
}

function execute_command() {
    const commandValue = command.value;
    setCursorBusy(true);
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", commandValue])
        .stream(execute_output)
        .then(() => {
            execute_success();
            setCursorBusy(false);
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });

    result.innerHTML = "";
    output.innerHTML = "";
}

function execute_success() {
    result.style.color = "green";
    result.innerHTML = "success";
}

function execute_fail() {
    result.style.color = "red";
    result.innerHTML = "fail";
}

function execute_output(data) {
    output.append(document.createTextNode(data));
}

function create_buttons() {
    const evenButtonsContainer = document.getElementById('even-buttons');
    const oddButtonsContainer = document.getElementById('odd-buttons');

    for (let i = 0; i < 48; i++) {
        const button = document.createElement('button');
        button.textContent = i;
        button.id = 'button-' + i;
        button.addEventListener('mouseover', () => {
            if (!isPopupSticky) show_interface_info(i);
        });
        button.addEventListener('mouseout', () => {
            if (!isPopupSticky) hide_popup();
        });
        button.addEventListener('click', () => {
            isPopupSticky = !isPopupSticky;
            if (isPopupSticky) {
                show_interface_info(i);
            }
        });
        if (i % 2 === 0) {
            evenButtonsContainer.appendChild(button);
        } else {
            oddButtonsContainer.appendChild(button);
        }
    }

    update_buttons_status();
    preload_interface_data();
}

function get_interface_status(interfaceInfo) {
    const statusLine = interfaceInfo.find(line => line.includes('Enabled') || line.includes('Administratively down'));
    return statusLine && statusLine.includes('Administratively down') ? 'Disabled' : 'Enabled';
}

function toggle_port(interfaceName) {
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", `show interface ${interfaceName}`])
        .then(data => {
            const interfaceInfo = data.split('\n');
            const status = get_interface_status(interfaceInfo);
            interfaceStatusCache[interfaceName] = status;
            if (status === 'Enabled') {
                disable_interface(interfaceName);
            } else if (status === 'Disabled') {
                enable_interface(interfaceName);
            }
            hide_popup();
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
}

function enable_interface(interfaceName) {
    setCursorBusy(true);
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", `configure; set interfaces ${interfaceName} enable; commit`])
        .then(() => {
            interfaceStatusCache[interfaceName] = 'Enabled';
            execute_success();
            setCursorBusy(false);
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
}

function disable_interface(interfaceName) {
    setCursorBusy(true);
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", `configure; set interfaces ${interfaceName} disable; commit`])
        .then(() => {
            interfaceStatusCache[interfaceName] = 'Disabled';
            execute_success();
            setCursorBusy(false);
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
}

function update_buttons_status() {
    setCursorBusy(true);
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", "show interfaces terse"])
        .then(data => {
            const lines = data.split('\n');
            const interfaceStatus = {};

            lines.forEach(line => {
                const parts = line.trim().split(/\s+/);
                if (parts.length > 2 && parts[0].match(/^ge-0\/0\/\d+$/)) {
                    const interfaceNum = parseInt(parts[0].split('/')[2]);
                    const status = parts[2]; // Link status (up or down)
                    interfaceStatus[interfaceNum] = status;
                }
            });

            for (let i = 0; i < 48; i++) {
                const button = document.getElementById('button-' + i);
                if (button) { // Check if the button exists
                    if (interfaceStatus[i] === 'up') {
                        button.style.backgroundColor = 'green';
                    } else if (interfaceStatus[i] === 'down') {
                        button.style.backgroundColor = 'red';
                    } else {
                        button.style.backgroundColor = 'grey';
                    }
                }
            }
            setCursorBusy(false);
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
}

function preload_interface_data() {
    setCursorBusy(true);
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", "show interfaces ge*"])
        .then(data => {
	    // uncomment to debug
            // console.log("Full interface data:", data); // Print the full interface data
            parse_interface_data(data);
            parse_interface_status(data);
            return cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", "show vlans"]);
         })
        .then(data => {
            // console.log("Full VLAN data:", data); // Print the full VLAN data
            parse_vlans_data(data);
            setCursorBusy(false);
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
}

function parse_interface_data(data) {
    const interfaces = data.split(/Physical interface: /).slice(1);
    interfaces.forEach(interfaceInfo => {
        const lines = interfaceInfo.trim().split('\n');
        const interfaceName = lines[0].split(',')[0].trim();
        interfaceCache[interfaceName] = interfaceInfo.trim();
    });
}

function parse_interface_status(data) {
    const interfaces = data.split(/Physical interface: /).slice(1);
    interfaces.forEach(interfaceInfo => {
        const lines = interfaceInfo.trim().split('\n');
        const interfaceName = lines[0].split(',')[0].trim();
        const statusLine = lines.find(line => line.includes('Enabled') || line.includes('Administratively down'));
        const status = statusLine.includes('Administratively down') ? 'Disabled' : 'Enabled';
        interfaceStatusCache[interfaceName] = status;
    });
}

function show_interface_info(port) {
    const interfaceName = `ge-0/0/${port}`;
    if (interfaceCache[interfaceName]) {
        create_popup(interfaceName, interfaceCache[interfaceName]);
    } else {
        setCursorBusy(true);
        cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", `show interface ${interfaceName}`])
            .then(data => {
                const cleanedData = data.replace("Physical interface: ", "").trim();
                interfaceCache[interfaceName] = cleanedData;
                create_popup(interfaceName, cleanedData);
                setCursorBusy(false);
            })
            .catch(() => {
                execute_fail();
                setCursorBusy(false);
            });
    }
}

function create_popup(interfaceName, data) {
    let popup = document.getElementById('popup');
    if (!popup) {
        popup = document.createElement('div');
        popup.id = 'popup';
        popup.className = 'popup';
        document.body.appendChild(popup);
    }

    const closeBtn = document.createElement('span');
    closeBtn.className = 'popup-close';
    closeBtn.innerHTML = '&times;';
    closeBtn.onclick = hide_popup;

    const content = document.createElement('div');
    content.className = 'popup-content';
    const interfaceDetails = data.split('\n');

    // Create interface info section
    const infoSection = document.createElement('div');
    infoSection.className = 'interface-info';

    const portNumber = document.createElement('p');
    portNumber.textContent = `Port: ${interfaceName}`;
    infoSection.appendChild(portNumber);

    const currentState = document.createElement('p');
    const state = interfaceStatusCache[interfaceName];
    currentState.textContent = `Current state: ${state ? state : 'Unknown'}`;
    infoSection.appendChild(currentState);

    const enableDisableButton = document.createElement('button');
    enableDisableButton.textContent = 'Enable/Disable';
    enableDisableButton.addEventListener('click', () => toggle_port(interfaceName));
    infoSection.appendChild(enableDisableButton);

    content.appendChild(infoSection);

    // Create VLAN controls
    const vlanSection = document.createElement('div');
    vlanSection.className = 'vlan-controls';
    vlanSection.innerHTML = `
        <label for="add-vlan">Add VLAN: </label>
        <input type="text" id="add-vlan" name="add-vlan">
        <button id="add-vlan-btn">Add</button>
        <br>
        <label>VLANs:</label>
        <ul id="vlan-list"></ul>
    `;

    content.appendChild(vlanSection);

    // Create Quarantine button
    const quarantineButton = document.createElement('button');
    quarantineButton.textContent = 'Quarantine';
    quarantineButton.className = 'quarantine-btn';
    quarantineButton.addEventListener('click', () => handleQuarantine(interfaceName));

    // Append quarantine button at the bottom of the popup
    content.appendChild(quarantineButton);

    popup.innerHTML = '';
    popup.appendChild(closeBtn);
    popup.appendChild(content);
    popup.style.display = 'block';

    // Event listeners for the VLAN controls and enable/disable slider
    document.getElementById('add-vlan-btn').addEventListener('click', () => handleVlanChange('add', interfaceName));
    // Populate VLAN list
    populate_vlan_list(interfaceName);
}

function handleQuarantine(interfaceName) {
    setCursorBusy(true);
    const command = `configure; set interfaces ${interfaceName} quarantine; commit`;
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", command])
    .then(() => {
        execute_success();
        setCursorBusy(false);
        // Additional actions can be added here if needed after quarantining
    })
    .catch(() => {
        execute_fail();
        setCursorBusy(false);
    });
}

function populate_vlan_list(interfaceName) {
    const vlanList = document.getElementById('vlan-list');
    if (vlanList && interfaceVlansCache) {
        vlanList.innerHTML = '';
        for (const vlanName in interfaceVlansCache) {
            const vlan = interfaceVlansCache[vlanName];
            if (vlan.interfaces.includes(interfaceName)) {
                const li = document.createElement('li');
                li.textContent = vlanName;

                // Create delete button
                const deleteButton = document.createElement('button');
                deleteButton.textContent = 'Delete';
                deleteButton.className = 'delete-btn';
                deleteButton.addEventListener('click', () => handleVlanDelete(interfaceName, vlanName));

                li.appendChild(deleteButton);
                vlanList.appendChild(li);
            }
        }
    }
}

function handleVlanDelete(interfaceName, vlanName) {
    setCursorBusy(true);
    const command = `remove vlan ${vlanName} from ${interfaceName}`;
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", command])
    .then(() => {
        execute_success();
        setCursorBusy(false);
        // Update the VLAN list after deleting a VLAN
        populate_vlan_list(interfaceName);
    })
    .catch(() => {
        execute_fail();
        setCursorBusy(false);
    });
}

function handleVlanChange(action, interfaceName) {
    const vlan = document.getElementById(`${action}-vlan`).value;
    if (vlan) {
        setCursorBusy(true);
        const command = action === 'add' ? `add vlan ${vlan} to ${interfaceName}` : `remove vlan ${vlan} from ${interfaceName}`;
        cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", command])
        .then(() => {
            execute_success();
            setCursorBusy(false);
            // Update the VLAN list after adding/removing a VLAN
            populate_vlan_list(interfaceName);
        })
        .catch(() => {
            execute_fail();
            setCursorBusy(false);
        });
    }
}

function handleEnableDisableChange(event, interfaceName) {
    const command = event.target.checked ? `enable ${interfaceName}` : `disable ${interfaceName}`;
    setCursorBusy(true);
    cockpit.spawn(["/opt/sbin/juniperswitch.py", "-c", command])
    .then(() => {
        execute_success();
        setCursorBusy(false);
    })
    .catch(() => {
        execute_fail();
        setCursorBusy(false);
    });
}

function hide_popup() {
    const popup = document.getElementById('popup');
    if (popup) {
        popup.style.display = 'none';
    }
}

// Run command if enter is pressed
command.addEventListener("keydown", function(event) {
    if (event.key === "Enter") {
        event.preventDefault();
        execute_command();
    }
});

button.addEventListener("click", execute_command);

cockpit.transport.wait(create_buttons);
cockpit.transport.wait(function() { });
