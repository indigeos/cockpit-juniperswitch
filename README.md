# Overview
This cockpit module controls Juniper EX2200 switches

# Installation
Close the respository in Cockpit root directory, usually `/usr/share/cockpit/`.

## Install python libraries
On Ubuntu, you may need to install the following (NOTE: this needs testing):

```
apt install python3-setuptools
apt install python3-pyasn1
apt install python-pip3
apt install python3-pip
apt install python-is-python3
apt install python3-paramiko
apt install python3-getpass
apt install python3-dotenv
```

# Create environment variables
NOTE: This should be done with more secure measures for password storage. TBD

./.env
```
HOST = "hostname"
USERNAME = "root"
PASSWORD = "password"
```

# Command strings

### Disable port
`configure; set interfaces ge-0/0/3 disable; commit`

### Enable port
`configure; set interfaces ge-0/0/3 enable; commit`

### Get full configuration
`configure; show`

### Show interfaces
`show interfaces`

### Show terse list of interfaces
`show interfaces terse | grep ge`

NOTE: This doesn't work because of the quotes and dollar signs
`show interfaces terse | grep ge | grep "up$|down$"`


